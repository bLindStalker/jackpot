package com.playtika.jackpot;

public class DaoServiceImpl implements DaoService {
    @Override
    public void save(User user) {
        System.out.println("user saved");
        updateCache(user);
    }

    public void updateCache(User user) {
        System.out.println("user cache updated");
    }
}
