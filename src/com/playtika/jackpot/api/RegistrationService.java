package com.playtika.jackpot.api;

import com.playtika.jackpot.User;

public interface RegistrationService {
    void registerUser(User user);
}
