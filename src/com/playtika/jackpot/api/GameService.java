package com.playtika.jackpot.api;

import com.playtika.jackpot.User;

public interface GameService {
    boolean play(User user);
}
