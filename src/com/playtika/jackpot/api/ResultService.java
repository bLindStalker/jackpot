package com.playtika.jackpot.api;

import com.playtika.jackpot.User;

public interface ResultService {
    void showResult(boolean result, User user);
}
