package com.playtika.jackpot;

public class User {
    protected final String name;
    protected final String secondName;
    private int number;
    private int doubleWinNumber;

    public User(String name, String secondName) {
        this.name = name;
        this.secondName = secondName;
    }

    public static User init(String[] arg) {
        return new User(arg[0], arg[1]);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDoubleWinNumber() {
        return doubleWinNumber;
    }

    public void setDoubleWinNumber(int doubleWinNumber) {
        this.doubleWinNumber = doubleWinNumber;
    }

    @Override
    public String toString() {
        return "'" + name + " " + secondName + "'";
    }
}
