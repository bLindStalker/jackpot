package com.playtika.jackpot.factories;

import com.playtika.jackpot.DaoServiceImpl;
import com.playtika.jackpot.LuckyNumberRegistrationService;
import com.playtika.jackpot.SimpleRegistrationService;
import com.playtika.jackpot.api.RegistrationService;

import java.util.Random;

import static com.playtika.jackpot.Utils.getProbability;
import static com.playtika.jackpot.Utils.isLuckyGame;

public class RegistrationFactory {

    public static RegistrationService initService() {
        return isLuckyGame()
                ? new LuckyNumberRegistrationService(new Random(), getProbability())
                : new SimpleRegistrationService(new DaoServiceImpl());
    }
}
