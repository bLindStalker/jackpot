package com.playtika.jackpot.factories;

import com.playtika.jackpot.LuckyNumberGameService;
import com.playtika.jackpot.SimpleGameService;
import com.playtika.jackpot.api.GameService;

import java.util.Random;

import static com.playtika.jackpot.Utils.getProbability;
import static com.playtika.jackpot.Utils.isLuckyGame;

public class GameFactory {

    public static GameService initService() {
        Random random = new Random();
        return isLuckyGame()
                ? new LuckyNumberGameService(random, getProbability())
                : new SimpleGameService(random);
    }
}
