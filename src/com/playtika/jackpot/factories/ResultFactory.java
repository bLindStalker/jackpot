package com.playtika.jackpot.factories;

import com.playtika.jackpot.SimpleResultService;
import com.playtika.jackpot.api.ResultService;

public class ResultFactory {

    public static ResultService initService() {
        return new SimpleResultService();
    }
}
