package com.playtika.jackpot;

import com.playtika.jackpot.api.ResultService;

public class SimpleResultService implements ResultService {

    @Override
    public void showResult(boolean result, User user) {
        if (result) {
            System.out.println("Congratulations " + user + "! You won!");
        } else {
            System.out.println("Unfortunately " + user + " you have lost, try again...");
        }
    }
}
