package com.playtika.jackpot;

public class Utils {
    public static boolean isLuckyGame() {
        String luckyGame = System.getenv("luckyGame");
        return luckyGame != null && Boolean.parseBoolean(luckyGame);
    }

    public static int getProbability() {
        return Integer.parseInt(System.getenv("luckyGameProbability"));
    }
}
