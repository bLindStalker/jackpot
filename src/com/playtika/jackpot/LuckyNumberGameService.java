package com.playtika.jackpot;

import com.playtika.jackpot.api.GameService;

import java.util.Random;

public class LuckyNumberGameService implements GameService {
    private Random random;
    private int probability;

    public LuckyNumberGameService(Random random, int probability) {
        this.random = random;
        this.probability = probability;
    }

    @Override
    public boolean play(User user) {
        System.out.println(String.format("%s starts the lucky number game!", user));
        int luckyNumber = random.nextInt(probability);
        System.out.println("Won number is: " + luckyNumber);
        boolean result = luckyNumber == user.getNumber();
        if (result && user.getNumber() == user.getDoubleWinNumber()) {
            System.out.println("You got double win!");
        }

        return result;
    }
}
