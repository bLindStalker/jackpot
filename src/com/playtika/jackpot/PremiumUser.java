package com.playtika.jackpot;

public class PremiumUser extends User {

    public PremiumUser(String name, String secondName) {
        super(name, secondName);
    }

    public static PremiumUser init(String[] arg) {
        return new PremiumUser(arg[0], arg[1]);
    }

    @Override
    public int getDoubleWinNumber() {
        return super.getNumber();
    }

    @Override
    public String toString() {
        return "Mr '" + name + " " + secondName + "'";
    }
}
