package com.playtika.jackpot;

import com.playtika.jackpot.api.GameService;
import com.playtika.jackpot.api.RegistrationService;
import com.playtika.jackpot.api.ResultService;
import com.playtika.jackpot.factories.GameFactory;
import com.playtika.jackpot.factories.RegistrationFactory;
import com.playtika.jackpot.factories.ResultFactory;

public class JackpotService {
    private static final RegistrationService registrationService = RegistrationFactory.initService();
    private static final GameService gameService = GameFactory.initService();
    private static final ResultService resultService = ResultFactory.initService();

    public static void main(String[] args) {
        //User user = User.init(args);
        User user = PremiumUser.init(args);

        registrationService.registerUser(user);

        boolean result = gameService.play(user);

        resultService.showResult(result, user);
    }
}
