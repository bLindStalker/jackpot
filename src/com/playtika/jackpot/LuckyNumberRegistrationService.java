package com.playtika.jackpot;

import com.playtika.jackpot.api.RegistrationService;

import java.util.Random;

public class LuckyNumberRegistrationService implements RegistrationService {
    private Random random;
    private int probability;

    public LuckyNumberRegistrationService(Random random, int probability) {
        this.random = random;
        this.probability = probability;
    }

    @Override
    public void registerUser(User user) {
        System.out.println(String.format("Welcome %s to the JACKPOT GAME!", user));
        int luckyNumber = random.nextInt(probability);
        user.setNumber(luckyNumber);
        user.setDoubleWinNumber(random.nextInt(probability));

        System.out.println(String.format("%s got %d lucky number", user, luckyNumber));
    }
}
