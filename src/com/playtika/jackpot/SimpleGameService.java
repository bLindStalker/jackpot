package com.playtika.jackpot;

import com.playtika.jackpot.api.GameService;

import java.util.Random;

public class SimpleGameService implements GameService {
    private Random random;

    public SimpleGameService(Random random) {
        this.random = random;
    }

    @Override
    public boolean play(User user) {
        System.out.println(String.format("%s starts the game!", user));
        return random.nextBoolean();
    }
}
