package com.playtika.jackpot;

import com.playtika.jackpot.api.RegistrationService;

public class SimpleRegistrationService implements RegistrationService {

    private final DaoService daoService;

    public SimpleRegistrationService(DaoService daoService) {
        this.daoService = daoService;
    }

    @Override
    public void registerUser(User user) {
        System.out.println(String.format("Welcome %s to the JACKPOT GAME!", user));
        daoService.save(user);
    }
}
